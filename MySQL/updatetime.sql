UPDATE room 
SET update_date = (
SELECT max(create_date) FROM post
WHERE room_id = ?
)
WHERE id = ?
