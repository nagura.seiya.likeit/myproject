CREATE DATABASE myproject_board DEFAULT CHARACTER SET utf8;

USE myproject_board;

--
--ユーザー情報テーブル
--
CREATE TABLE user(
id SERIAL,
login_id varchar(100) UNIQUE NOT NULL,
name varchar(30) NOT NULL,
password varchar(100) NOT NULL,
file_name varchar(256) default 'noImage.png',
create_date DATETIME default now(),
update_date DATETIME default now())
;

--
--チャットルーム情報テーブル
--
CREATE TABLE room(
id SERIAL,
creator_id int NOT NULL,
name varchar(255) NOT NULL,
room_detail text NOT NULL,
create_date DATETIME default now(),
update_date DATETIME default now())
;

--
--投稿情報テーブル
--
CREATE TABLE post(
id SERIAL,
room_id int NOT NULL,
contributor_id int NOT NULL,
content text NOT NULL,
create_date DATETIME default now(),
update_date DATETIME default now())
;

