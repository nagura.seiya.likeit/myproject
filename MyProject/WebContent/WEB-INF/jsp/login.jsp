<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<br>
	<div class="container">
		<div class="col">
			<h1 style="text-align: center">ログイン</h1>
			<br> <br>
		</div>
	</div>
	<br>


	<!-- エラーメッセージの処理 -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<!-- フォーム -->
	<form action="LoginServlet" method="post">

		<div class="container">
			<div class="row">
				<div class="col-3"></div>
				<div class="form-group col-6">
					<label for="name">ログインID</label> <input type="text" name="loginId"
						class="form-control">
				</div>
				<div class="col-3"></div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-3"></div>
				<div class="form-group col-6">
					<label for="password">パスワード</label> <input type="password"
						name="password" class="form-control">
				</div>
				<div class="col-3"></div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-5"></div>
				<button type="submit" class="btn btn-primary btn-block">ログイン</button>
				<div class="col-5"></div>
			</div>
		</div>
	</form>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-5"></div>

			<button onclick="location.href='NewUserServlet'"
				class="btn btn-outline-primary btn-block">新規登録</button>
			<div class="col-5"></div>
		</div>
	</div>
</body>
</html>
