<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>チャットルーム名</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">



</head>
<body>


	<header style="background-color: snow">

		<div class="row">
			<div class="col-4">
				<div class="dropdown">
					<button type="button" id="dropdown1"
						class="btn btn-success dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">メニュー</button>
					<div class="dropdown-menu" aria-labelledby="dropdown1">
						<a class="dropdown-item" href="MyPageServlet">マイページ</a> <a
							class="dropdown-item" href="NewRoomServlet">新規チャットルーム作成</a> <a
							class="dropdown-item" href="LogoutServlet">ログアウト</a>
					</div>
				</div>
			</div>
			<div class="col-">
				<h4 style="color: black">${room.name}</h4>
			</div>
		</div>

	</header>

	<!--入力フォームの呼び出し-->
	<button class="btn btn-info" data-toggle="collapse"
		data-target="#example-1" aria-expand="false" aria-controls="example-1">メッセージを作成</button>
	<!-- フォーム -->
	<form action="TimelineServlet" method="post">
		<!-- ユーザーidとルームidの情報 -->
		<input type="hidden" name="roomId" value="${room.id}"> <input
			type="hidden" name="userId" value="${userInfo.id}">

		<div class="collapse" id="example-1">
			<div class="card card-body">
				<div class="form-group">
					<textarea id="textarea" name="content" class="form-control"
						placeholder="メッセージを入力"></textarea>
					<button type="submit" class="btn btn-outline-info">書き込む</button>
				</div>
			</div>
		</div>
	</form>

	<table class="table">

		<tr>
			<td><strong>ルーム作成者:${room.creatorBeans.name}</strong><br>
				${room.createDate}<br> ${room.roomDetail}</td>
		</tr>
	</table>
	<!-- 投稿テーブル -->
	<table class="table">
		<c:forEach var="post" items="${postList}">
			<tr>
				<td><strong>${post.contributer.name}</strong>：${post.createDate}<br>
					${post.content}</td>
			</tr>
		</c:forEach>
	</table>

	<footer style="background-color: snow">
		<div class="col row">
			<div class="col">
				<a href="TimelineListServlet">チャットルーム一覧に戻る</a>
			</div>
		</div>
	</footer>

	<!-- jQuery、Popper.js、Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>

<!--
-->
