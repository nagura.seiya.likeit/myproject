<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header style="background-color: snow">
		<div class="mx-auto" style="width: 290px">
			<h4 style="color: black">チャットアプリへようこそ</h4>
		</div>
	</header>

	<br>

	<!-- エラーメッセージの処理 -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<!-- フォーム -->

	<form action="NewUserServlet" method="post" enctype="multipart/form-data">

		<div class="mx-auto" style="width: 500px">
			<div class="form-group">
				<label for="loginId">ログインID</label> <input type="text" name="loginId"
					class="form-control">
			</div>



			<div class="form-group">
				<label for="user_name">ニックネーム</label> <input type="text"
					name="name" class="form-control">
			</div>


			<div class="form-group">
				<label for="passwd1">パスワード</label> <input type="password"
					name="password1" class="form-control">
			</div>


			<div class="form-group">
				<label for="passwd2">パスワード(確認)</label> <input type="password"
					name="password2" class="form-control">
			</div>


			<div class="element_wrap">
		     <label>プロフィール写真の設定</label>
		     <br>
		      <input type="file" name="profPicture">
	        </div>
	        <br>

			<div class="container">
				<div class="row">
					<div class="col-5"></div>
					<button type="submit" class="btn btn-info btn-block">登録</button>
					<div class="col-5"></div>
				</div>
			</div>
		</div>
	</form>



	<br>
	<br>
	<br>
	<div class="col row">
		<div class="col">
			<a href="LoginServlet">トップページ</a>
		</div>
	</div>
</body>
</html>