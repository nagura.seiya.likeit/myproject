<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>チャットルーム作成</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header style="background-color: snow">
		<div class="mx-auto" style="width: 350px">
			<h4 style="color: black">新しいチャットルームの作成</h4>
		</div>
	</header>

	<br>

	<!-- フォーム -->
	<form action="NewRoomServlet" method="post">


		<div class="mx-auto" style="width: 500px">

			<div class="form-group">
				<label for="room_name">ルーム名</label> <input type="text" name="name"
					class="form-control">
			</div>

			<div class="form-group">
				<label for="room_detail">ルーム詳細</label> <br>
				<textarea name="detail" cols="70" rows="3"></textarea>
			</div>

			<br>

			<div class="container">
				<div class="row">
					<div class="col-5"></div>
					<button type="submit" class="btn btn-info btn-block">チャットルームを作成する</button>
					<div class="col-5"></div>
				</div>
			</div>
		</div>
	</form>

	<br>
	<div class="col row">
		<div class="col">
			<a href="TimelineListServlet">チャットルーム一覧へ戻る</a>
		</div>
	</div>
</body>
</html>