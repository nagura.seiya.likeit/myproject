<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>チャット一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">



</head>
<body>


	<header style="background-color: snow">

		<div class="row">
			<div class="col-4">
				<div class="dropdown">
					<button type="button" id="dropdown1"
						class="btn btn-success dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">メニュー</button>
					<div class="dropdown-menu" aria-labelledby="dropdown1">
						<a class="dropdown-item" href="MyPageServlet">マイページ</a> <a
							class="dropdown-item" href="NewRoomServlet">新規チャットルーム作成</a> <a
							class="dropdown-item" href="LogoutServlet">ログアウト</a>
					</div>
				</div>
			</div>
			<div class="col-">
				<h4 style="color: black">チャットルーム一覧</h4>
			</div>
		</div>


	</header>
	<!-- 検索フォーム -->
	<form action="TimelineListServlet" method="post">
		<div class="container">
			<div class="row">
				<div class="col">タイトル名検索</div>

				<div class="input-group mb-2">
					<input type="text" name="inputRoomName" class="form-control">
					<div class="input-group-append">
						<button type="submit" class="btn btn-info">検索</button>
					</div>
				</div>
			</div>
		</div>

	</form>

	<!-- ルーム一覧の表示 -->
	<div class="list-group">
		<c:forEach var="room" items="${roomList}">
			<a href="TimelineServlet?id=${room.id}"
				class="list-group-item list-group-item-action">
				<strong>${room.name}</strong><br>
				${room.roomDetail}<br>
				更新：${room.updateDate}
				</a>
		</c:forEach>
	</div>

	<footer style="background-color: snow">
		<br>
	</footer>


	<!-- jQuery、Popper.js、Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>


</body>
</html>


<!--
<button type="button" class="btn btn-outline-dark" style="position: absolute; right: 20px; bottom: 20px"　
        data-toggle="modal" data-target="#modal1">メッセージの作成</button>

<div class="modal fade" id="modal1" tabindex="-1"
      role="dialog" aria-labelledby="label1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label1">メッセージを作成</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
  　　　　　　　<input type="text" value=""placeholder="メッセージを入力" id="message" class="form-control">
　　　　　</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">送信</button>
      </div>
    </div>
  </div>
</div>
-->