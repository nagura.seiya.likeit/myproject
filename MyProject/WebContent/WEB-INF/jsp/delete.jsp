<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
<body>

	<header style="background-color: snow">

		<div class="container">
         <div class="row">
			<div class="col"></div>
			<div class="col-">
				<h4 style="color: black">退会</h4>
			</div>
            <div class="col"></div>
         </div>
		</div>

	</header>

          <p class="h5 text-center">
          ログインID: ${userInfo.loginId}</p>
          <p class="h5 text-center">
          ${userInfo.name}さん
          </p>

    <br>
          <p class="h5 text-center">
          退会処理はキャンセルできません。
      <br>よろしいでしょうか？
          </p>


    <br>
    <br>
    <br>

<div class="container">
  <div class="row">

    <div class="col"></div>
    <div class="col">
    <button type="button" class="btn btn-outline-danger btn-lg"
            onclick="location.href='MyPageServlet'">
            キャンセル
    </button>
    </div>
    <div class="col">
    <form action="DeleteServlet" method="post">
        <input type="hidden" name="loginId" value="${userInfo.loginId}">
        <input type="submit" class="btn btn-danger btn-lg" value="退会">
        </form>
    </div>
  </div>
</div>

</body>
</html>