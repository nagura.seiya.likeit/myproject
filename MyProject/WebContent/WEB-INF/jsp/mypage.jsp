<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>マイページ</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
<body>

	<header style="background-color: snow">

		<div class="container">
         <div class="row">
			<div class="col"></div>
			<div class="col">
				<h4 style="color: black">マイページ</h4>
			</div>
			<a href="LogoutServlet" style="color: red"> ログアウト </a>

         </div>
		</div>

	</header>

    <br>
    <h5 style="text-align: center">${userInfo.name}さんのプロフィール</h5>
    <br>

      <div class="col">
        <img src="img/${userInfo.fileName}" width="100" height="100" class="rounded mx-auto d-block img-thumbnail">
        </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-3">ログインID</div>
      <div class="col">${userInfo.loginId}</div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-3">ニックネーム</div>
      <div class="col">${userInfo.name}</div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-3">登録日時</div>
      <div class="col">${userInfo.createDate}</div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-3">更新日時</div>
      <div class="col">${userInfo.updateDate}</div>
    </div>
    <br>
    <div class="cotainer">
        <div class="row">
            <div class="col"></div>
            <div class="col"></div>
            <div class="col"></div>
            <div class="col">
         <button class="btn btn-outline-info" onclick="location.href='UpdateServlet'">プロフィールの変更</button>
            </div>
            <div class="col">
         <button class="btn btn-outline-danger" onclick="location.href='DeleteServlet'">退会</button>
            </div>
            <div class="col"></div>
            <div class="col"></div>
            <div class="col"></div>
        </div>
    </div>

    <br>
    <br>
    <br>
 <div class="col row">
     <div class="col">
    <a href="TimelineListServlet">チャットルーム一覧へ</a>
     </div>
 </div>
</body>
</html>