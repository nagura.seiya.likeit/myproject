<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>プロフィールの変更</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header style="background-color: snow">

		<div class="container">
			<div class="row">
				<div class="col"></div>
				<div class="col-">
					<h4 style="color: black">プロフィール変更</h4>
				</div>
				<div class="col"></div>
			</div>
		</div>

	</header>

	<br>
	<br>


	<img src="img/${userInfo.fileName}" width="100" height="100"
		class="rounded mx-auto d-block img-thumbnail">

	<!-- フォーム -->
	<form action="UpdateServlet" method="post"
		enctype="multipart/form-data">
		<!-- id情報 -->
		<input type="hidden" name="loginId" value="${userInfo.loginId}">

		<div class="mx-auto" style="width: 500px">
			<div class="form-group">
				<label for="loginId">ログインID：</label>${userInfo.loginId}
			</div>



			<div class="form-group">
				<label for="user_name">ニックネーム</label> <input type="text"
					name="name" value="${userInfo.name}" class="form-control">
			</div>


			<div class="form-group">
				<label for="passwd1">パスワード</label> <input type="password"
					name="password1" class="form-control">
			</div>


			<div class="form-group">
				<label for="passwd2">パスワード(確認)</label> <input type="password"
					name="password2" class="form-control">
			</div>


			<div class="element_wrap">
		     <label>プロフィール写真の設定</label>
		     <br>
		      <input type="file" name="profPicture">
	        </div>
	        <br>

			<div class="container">
				<div class="row">
					<div class="col-5"></div>
					<button type="submit" class="btn btn-info btn-block">更新する</button>
					<div class="col-5"></div>
				</div>
			</div>
		</div>
	</form>

	<br>
	<br>
	<br>
	<div class="col row">
		<div class="col">
			<a href="MyPageServlet">戻る</a>
		</div>
	</div>
</body>
</html>