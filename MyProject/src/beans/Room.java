package beans;

import java.io.Serializable;

import dao.UserDao;

public class Room implements Serializable {
	private int id;
	private int creatorId;
	private String name;
	private String roomDetail;
	private String createDate;
	private String updateDate;

	public Room() {

	}

	//全データをセット
	public Room(int id, int creatorId, String name, String roomDetail, String createDate, String updateDate) {
		this.id = id;
		this.creatorId = creatorId;
		this.name = name;
		this.roomDetail =roomDetail;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCreatorId() {
		return creatorId;
	}


	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoomDetail() {
		return roomDetail;
	}

	public void setRoomDetail(String roomDetail) {
		this.roomDetail = roomDetail;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	// 結合テーブル情報
	public User getCreatorBeans() {
		UserDao dao = new UserDao();
		User user = dao.newUserLogin(this.creatorId);
		return user;
	}

}