package beans;

import java.io.Serializable;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private String fileName;
	private String password;
	private String createDate;
	private String updateDate;

	public User() {

	}

	//ログインセッションの保存
	public User(int id, String loginId, String name, String fileName, String createDate, String updateDate) {
		this.setId(id);
		this.setLoginId(loginId);
		this.setName(name);
		this.setFileName(fileName);
		this.setCreateDate(createDate);
		this.setUpdateDate(updateDate);
	}

	//全データをセット
	public User(int id, String loginId, String name, String fileName, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.fileName = fileName;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
