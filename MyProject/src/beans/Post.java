package beans;

import java.io.Serializable;

import dao.UserDao;

public class Post implements Serializable {
	private int id;
	private int roomId;
	private int contributerId;
	private String content;
	private String createDate;
	private String updateDate;

	public Post() {

	}

	//全データをセット
	public Post(int id, int roomId, int contributerId, String content, String createDate, String updateDate) {
		this.id = id;
		this.roomId = roomId;
		this.contributerId = contributerId;
		this.content = content;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public int getContributerId() {
		return contributerId;
	}

	public void setContributerId(int contributerId) {
		this.contributerId = contributerId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	// 結合テーブル情報
	public User getContributer() {
		UserDao userDao = new UserDao();
		User user = userDao.newUserLogin(this.contributerId);
		return user;
	}
}