package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.bind.DatatypeConverter;

import beans.User;

public class UserDao {

	/**ログイン情報の検索**/
	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, Encryption(password));

			ResultSet rs = stmt.executeQuery();

			//ログイン失敗時
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時
			int idData = rs.getInt("id");
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String fileName = rs.getString("file_name");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idData,loginidData, nameData, fileName, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}


	/**新規登録後、IDを引数にしてログイン情報を探す**/
	public User newUserLogin(int id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String fileName = rs.getString("file_name");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idData,loginidData, nameData, fileName, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**更新後、ログインIDを引数にしてログイン情報を探す**/
	public User AfterUpdate(String loginId) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);

			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String fileName = rs.getString("file_name");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idData,loginidData, nameData, fileName, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**ユーザー情報の登録**/
	public int InsertUser(String loginId, String name, String password, String fileName) {
		Connection conn = null;
		PreparedStatement stmt = null;
		int autoIncKey = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id,name,password,file_name,create_date,update_date) VALUES (?,?,?,?,now(),now())";

			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, Encryption(password));
			stmt.setString(4, fileName);

			//SQLの実行
			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return autoIncKey;
	}

	/**ユーザー情報の更新(写真あり)**/
	public User UpdateUser(String name, String fileName, String password, String loginId) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name= ?, file_name= ?, password= ?, update_Date= now() WHERE login_id= ?";
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, name);
			stmt.setString(2, fileName);
			stmt.setString(3, Encryption(password));
			stmt.setString(4, loginId);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**ユーザー情報の更新(写真なし)**/
	public User UpdateUser(String name, String password, String loginId) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name= ?, password= ?, update_Date= now() WHERE login_id= ?";
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, name);
			stmt.setString(2, Encryption(password));
			stmt.setString(3, loginId);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**ユーザーデータの削除**/
	public User Delete(String loginId) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "DELETE FROM user WHERE login_id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	/**暗号化の処理**/
	public String Encryption(String password) {
		//ハッシュを生成したい元の文字列
		String source = "password";

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		return result;
	}

}
