package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Post;

public class PostDao {

	/**部屋が持ってる投稿データを全て読み込む**/
	public List<Post> findPost(String roomId) {
		Connection conn = null;
		List<Post> postList = new ArrayList<Post>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM post WHERE room_id = ? ORDER BY create_date DESC";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, roomId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				int id = rs.getInt("id");
				int roomId2 = rs.getInt("room_id");
				int contributerId = rs.getInt("contributor_id");
				String content = rs.getString("content");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Post post = new Post(id, roomId2, contributerId, content, createDate, updateDate);
				postList.add(post);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return postList;

	}

	/**投稿データの登録**/
	public int InsertPost(int roomId, int contributorId, String content) {
		Connection conn = null;
		PreparedStatement stmt = null;
		int autoIncKey = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO post (room_id,contributor_id,content,create_date,update_date) VALUES (?,?,?,now(),now())";

			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stmt.setInt(1, roomId);
			stmt.setInt(2, contributorId);
			stmt.setString(3, content);

			//SQLの実行
			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return autoIncKey;
	}

	/**投稿した後にroomの更新日時を最新のものにする**/
	public Post UpdateTime(int roomId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE room"
					+ " SET update_date = ("
					+ " SELECT max(create_date) FROM post"
					+ " WHERE room_id = ?) WHERE id = ?";

			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, roomId);
			stmt.setInt(2, roomId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**投稿データの削除**/
	public Post DeleteContribute(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "DELETE FROM post WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

}
