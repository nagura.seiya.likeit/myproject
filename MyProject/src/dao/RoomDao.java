package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Room;

public class RoomDao {

	/**ルームデータを全て取得して更新日順に並べる**/
	public List<Room> findAll() {

		Connection conn = null;
		List<Room> roomList = new ArrayList<Room>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM room ORDER BY update_date DESC";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				int id = rs.getInt("id");
				int creatorId = rs.getInt("creator_id");
				String name = rs.getString("name");
				String roomDetail = rs.getString("room_detail");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Room room = new Room(id, creatorId, name, roomDetail, createDate, updateDate);

				roomList.add(room);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return roomList;
	}

	/**ルームIDをもとにデータを取得**/
	public Room findroomData(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM room WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id2 = rs.getInt("id");
			int creatorId = rs.getInt("creator_id");
			String name = rs.getString("name");
			String roomDetail = rs.getString("room_detail");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			Room room = new Room(id2, creatorId, name, roomDetail, createDate, updateDate);

			return room;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	/**新規ルームの登録**/
	public Room InsertRoom(int creatorId, String name, String roomDetail) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO room (creator_id,name,room_detail,create_date,update_date) VALUES (?,?,?,now(),now())";

			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, creatorId);
			stmt.setString(2, name);
			stmt.setString(3, roomDetail);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**ルーム名検索**/
	public List<Room> SeachRoom(String inputRoomName) {
		Connection conn = null;
		PreparedStatement stmt = null;
		List<Room> roomList = new ArrayList<Room>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM room WHERE name LIKE ? ORDER BY update_date DESC";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, "%" + inputRoomName + "%");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				int id = rs.getInt("id");
				int creatorId = rs.getInt("creator_id");
				String name = rs.getString("name");
				String roomDetail = rs.getString("room_detail");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Room room = new Room(id, creatorId, name, roomDetail, createDate, updateDate);

				roomList.add(room);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return roomList;
	}

	/**選択したルームの削除**/
	public Room DeleteRoom(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "DELETE FROM room WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

}
