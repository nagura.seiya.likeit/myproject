package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Room;
import dao.RoomDao;

/**
 * Servlet implementation class TimelineListServlet
 */
@WebServlet("/TimelineListServlet")
public class TimelineListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TimelineListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ルームデータを取得してからフォワード
		RoomDao roomDao = new RoomDao();
		List<Room> roomList = roomDao.findAll();

		request.setAttribute("roomList", roomList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/**検索処理**/
		//リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		// リクエストパラメータの入力項目を取得
		String inputRoomName = request.getParameter("inputRoomName");
		//検索結果をセット
		RoomDao roomDao = new RoomDao();
		List<Room> result = roomDao.SeachRoom(inputRoomName);
		request.setAttribute("roomList", result);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline_list.jsp");
		dispatcher.forward(request, response);
	}

}
