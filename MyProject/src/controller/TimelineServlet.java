package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Post;
import beans.Room;
import beans.User;
import dao.PostDao;
import dao.RoomDao;

/**
 * Servlet implementation class TimelineServlet
 */
@WebServlet("/TimelineServlet")
public class TimelineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TimelineServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン時のセッションを取得
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		//セッションがない場合はログイン画面へ
		if (user == null) {
			request.setAttribute("errMsg", "ログインしてください");
			//フォワード(ログイン画面に戻る)
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// URLからGETパラメータとしてルームIDを受け取る
		String roomId = request.getParameter("id");

		// roomIdを引数にして、idに紐づくroom情報を出力する
		RoomDao roomDao = new RoomDao();
		Room room = roomDao.findroomData(roomId);
		request.setAttribute("room", room);
		// roomIdを引数にして、idに紐づくpost情報を出力する
		PostDao postdao = new PostDao();
		List<Post> postList = postdao.findPost(roomId);
		request.setAttribute("postList", postList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		int roomId = Integer.parseInt(request.getParameter("roomId"));
		int userId = Integer.parseInt(request.getParameter("userId"));
		String content = request.getParameter("content");

		//書き込み処理(投稿IDの取得)
		PostDao postDao = new PostDao();
		postDao.InsertPost(roomId, userId, content);

		//更新日時を最新のものにする
		PostDao dao = new PostDao();
		dao.UpdateTime(roomId);

		//ルームへリダイレクト
		response.sendRedirect("TimelineServlet?id="+roomId);

	}

}
