package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class NewUser
 */
@WebServlet("/NewUserServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-QQASV86.000.001\\Documents\\myproject\\MyProject\\WebContent\\img", maxFileSize = 1048576)
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		Part part = request.getPart("profPicture");
		String fileName = this.getFileName(part);
		part.write(fileName);

		//写真を設定しない場合はデフォルトアイコンに設定
		if (fileName.equals("")) {
			fileName = "noImage.png";
		}

		//空欄がある時
		if (loginId == "" || name == "" || password1 == "") {
			request.setAttribute("errMsg", "未入力の項目があります");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (!password1.equals(password2)) {
			request.setAttribute("errMsg", "パスワードを確認してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/**登録時にIDを受け取り、ログイン状態にする**/
		UserDao userDao = new UserDao();
		int i = userDao.InsertUser(loginId, name, password1, fileName);

		UserDao userDao2 = new UserDao();
		User user2 = userDao2.newUserLogin(i);

		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user2);

		//マイページへリダイレクト
		response.sendRedirect("MyPageServlet");

	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
