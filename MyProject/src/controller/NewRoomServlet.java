package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.RoomDao;

/**
 * Servlet implementation class NewRoomServlet
 */
@WebServlet("/NewRoomServlet")
public class NewRoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewRoomServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン時のセッションを取得
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		//セッションがない場合はログイン画面へ
		if (user == null) {
			request.setAttribute("errMsg", "ログインしてください");
			//フォワード(ログイン画面に戻る)
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newroom.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//セッションから作成者のIDを取得
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		int creatorId = user.getId();

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String roomDetail = request.getParameter("detail");

		//タイトル欄が空欄の時
		if (name.equals("")) {
			request.setAttribute("errMsg", "タイトルを入力してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newroom.jsp");
			dispatcher.forward(request, response);
			return;
		}
		RoomDao roomDao = new RoomDao();
		roomDao.InsertRoom(creatorId, name, roomDetail);

		//作成した部屋へリダイレクト
		//(テスト)ルーム一覧へ
		response.sendRedirect("TimelineListServlet");
	}

}
